<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateRangeFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder

            ->add('bank', EntityType::class,[
                'class' => Bank::class,
                'placeholder' => 'Select Bank',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ],
                'required' => false
            ])

            ->add('agentid', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])

            ->add('region', EntityType::class,[
                'class' => Location::class,
                'placeholder' => 'Select Region',
                'choice_label' => 'name',
                'required' => false,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1')
                        ->where('e.level = 3')
                        ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])

            ->add('accountType',ChoiceType::class,[
                'choices' => [
                    'Poultry' => 'POULTRY',
                    'Agro' => 'AGRO',
                    'Feeds' => 'FEED'
                ],
                'placeholder' => 'Select Account Type',
//                'attr' => [
//                    'class' => 'select2'
//                ],
                'required' => false
            ])

            ->add('startDate', TextType::class,[
                'attr' => [
                    'placeholder' => 'dd-mm-YYYY',
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])

            ->add('endDate', TextType::class,[
                'attr' => [
                    'placeholder' => 'dd-mm-YYYY',
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])

            ->add('Submit', SubmitType::class)
            ->setMethod('get')
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }



}