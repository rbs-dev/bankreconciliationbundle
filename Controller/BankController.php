<?php


namespace Terminalbd\BankReconciliationBundle\Controller;


use App\Entity\Admin\Bank;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Form\BankFormType;

/**
 * Class BankController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
 */
class BankController extends AbstractController
{
    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/bank-list", name="br_bank_list")
     */
    public function index(Request $request)
    {
        $records = $this->getDoctrine()->getRepository(Bank::class)->findBy(['status'=>1]);
        $data = $this->paginate($request, $records);

        $bank = new Bank();
        $form = $this->createForm(BankFormType::class, $bank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $bank->setName($form['name']->getData());
            $bank->setDeductionPercentage($form['deductionPercentage']->getData());
            $bank->setCreatedAt(new \DateTime('now'));
//            $bank->setStatus($form['status']->getData());
            $bank->setStatus(1);
            $em->persist($bank);
            $em->flush();
            $this->addFlash('success', 'New Bank added!');
        }

        return $this->render('@TerminalbdBankReconciliation/bank/index.html.twig',[
            'data' => $data,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @param Request $request
     * @Route("/bank/{id}/update", name="br_bank_update")
     */
    public function bankUpdate(Request $request,Bank $bank)
    {
        $records = $this->getDoctrine()->getRepository(Bank::class)->findBy(['status'=>1]);
        $data = $this->paginate($request, $records);

        $form = $this->createForm(BankFormType::class, $bank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $bank->setName($form['name']->getData());
            $bank->setDeductionPercentage($form['deductionPercentage']->getData());
            $bank->setUpdatedAt(new \DateTime('now'));
//            $bank->setStatus($form['status']->getData());
            $bank->setStatus(1);
            $em->persist($bank);
            $em->flush();

            $this->addFlash('success', 'Bank Updated Successfully!');
            return $this->redirectToRoute('br_bank_list');

        }
        return $this->render('@TerminalbdBankReconciliation/bank/index.html.twig',[
            'form' => $form->createView(),
            'data' => $data
        ]);
    }


}