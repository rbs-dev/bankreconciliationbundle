<?php


namespace Terminalbd\BankReconciliationBundle\Entity;

use App\Entity\Core\Agent;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\SalesPaymentRepository")
 * @ORM\Table(name="br_sales_payment")
*/
class SalesPayment
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Agent")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $agent;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankBranch", inversedBy="branch")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $branch;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\FileUpload", inversedBy="transactions")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="cascade")
     */
    private $fileUpload;


    /**
     * @var $bankReconciliation
     * @ORM\OneToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\Reconciliation",mappedBy="salesPayment")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $bankReconciliation;

    /**
     * @var $bankTransaction
     * @ORM\OneToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankTransaction")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $bankTransaction;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $depositAmount;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentId;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $depositDate;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $status;



    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true)
     */
    private $isSales = 'no';

    /**
     * @return string
     */
    public function getIsSales()
    {
        return $this->isSales;
    }

    /**
     * @param string $isSales
     */
    public function setIsSales(string $isSales): void
    {
        $this->isSales = $isSales;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @return string
     */
    public function getBank(): string
    {
        return $this->bank;
    }

    /**
     * @param string $bank
     */
    public function setBank(string $bank): void
    {
        $this->bank = $bank;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch): void
    {
        $this->branch = $branch;
    }

    /**
     * @return mixed
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @param mixed $fileUpload
     */
    public function setFileUpload($fileUpload): void
    {
        $this->fileUpload = $fileUpload;
    }


    /**
     * @return \DateTime
     */
    public function getDepositDate()
    {
        return $this->depositDate;
    }

    /**
     * @param \DateTime $depositDate
     */
    public function setDepositDate($depositDate)
    {
        $this->depositDate = $depositDate;
    }


    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return Reconciliation
     */
    public function getBankReconciliation()
    {
        return $this->bankReconciliation;
    }

    /**
     * @param Reconciliation $bankReconciliation
     */
    public function setBankReconciliation($bankReconciliation): void
    {
        $this->bankReconciliation = $bankReconciliation;
    }

    /**
     * @return BankTransaction
     */
    public function getBankTransaction()
    {
        return $this->bankTransaction;
    }

    /**
     * @param BankTransaction $bankTransaction
     */
    public function setBankTransaction($bankTransaction): void
    {
        $this->bankTransaction = $bankTransaction;
    }


    /**
     * @return float
     */
    public function getDepositAmount()
    {
        return $this->depositAmount;
    }

    /**
     * @param float $depositAmount
     */
    public function setDepositAmount(float $depositAmount)
    {
        $this->depositAmount = $depositAmount;
    }

    /**
     * @return int
     */
    public function getPaymentId(): int
    {
        return $this->paymentId;
    }

    /**
     * @param int $paymentId
     */
    public function setPaymentId(int $paymentId): void
    {
        $this->paymentId = $paymentId;
    }







}