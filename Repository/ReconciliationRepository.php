<?php

namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;


class ReconciliationRepository extends EntityRepository
{
    public function CompanyWiseTotalValue($transactionDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.fileUpload','fileUpload');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('fileUpload.bank', 'bank');
        $qb->leftJoin('bank.accountCode','accountCode');
//        $qb->select('SUM(e.salesPaymentAmount) as DepositeAmount');
        $qb->select('e.salesPaymentAmount as DepositeAmount');
        $qb->addSelect('bank.name as BankName');
        $qb->addSelect('accountCode.accountType as accountType');
//        $qb->addSelect('fileUpload.accountType');
        $qb->addSelect('bankTransaction.transactionDate as transactionDate');
        $qb->where('e.approvedBy IS NOT NULL');
        $qb->andWhere('bankTransaction.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);
//        $qb->groupBy('e.fileUpload');
//        $qb->groupBy('fileUpload.accountType');
//        $qb->groupBy('fileUpload.bank');
//        $qb->groupBy('accountCode.accountType');
        $qb->groupBy('accountCode.id');
        $qb->orderBy('bank.name','ASC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }
    /*public function getDailyReconciliationReportDataByUploadID($uploadID){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('e.fileUpload', 'fileUpload');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');
        $qb->leftJoin('BankAccountCode.bank', 'bank');


        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag','e.isDownload');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');

        $qb->where('e.fileUpload = :fileUpload')->setParameter('fileUpload', $uploadID);
        $qb->andWhere('e.approvedBy IS NOT NULL');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }*/

    public function getDailyReconciliationReportData($bank,$type,$transactionDate,$reportType){
//        dd($bank,$type,$transactionDate,$reportType);
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');
        $qb->leftJoin('BankAccountCode.bank', 'bank');

        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag','e.isDownload');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.reportDate as reportDate','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');
        $qb->addSelect('fileUpload.accountType as accountType');

       // $qb->where('fileUpload.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);
        $qb->where('bankTransaction.reportDate = :reportDate')->setParameter('reportDate',$transactionDate);
        if (isset($type)){
            $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType', $type);
        }
        $qb->andWhere('e.approvedBy IS NOT NULL');
        if ($bank){
            $qb->andWhere('bankTransaction.bank = :bank')->setParameter('bank',$bank);
        }
        if ($reportType == 'backdate'){
            $qb->andWhere('bankTransaction.transactionDate != :transactionDate')->setParameter('transactionDate', $transactionDate);
        }
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function reconcialiationData($uploadID){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');

        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy1');
        $qb->leftJoin('e.fileUpload', 'fileUploadData');
        $qb->leftjoin('fileUploadData.bank', 'bank');

        $qb->leftJoin('fileUploadData.BankAccountCode', 'bankAccountCode');

        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag');
//        $qb->addSelect('branch.branchCode','branch.id as branchId','branch.branchName as branchName');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description');
//        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('approvedBy1.id AS approvedBy');
        $qb->addSelect('bankAccountCode.accountCode');
        $qb->addSelect('bank.id as bankId','bank.name as bankName');

        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');

        $qb->where('e.fileUpload = :fileUpload')->setParameter('fileUpload', $uploadID);
        $qb->andWhere('e.bankTransaction IS NOT NULL');
        $qb->orderBy('e.reconciliationFlag');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function reconcialiationDataWithoutBank($type,$date){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');

        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy1');
        $qb->leftJoin('e.fileUpload', 'fileUploadData');
        $qb->leftJoin('fileUploadData.BankAccountCode', 'bankAccountCode');
        $qb->leftjoin('fileUploadData.bank', 'bank');


        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag');
//        $qb->addSelect('branch.branchCode','branch.id as branchId','branch.branchName as branchName');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description');
//        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('approvedBy1.id AS approvedBy');
        $qb->addSelect('bankAccountCode.accountCode');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('bank.id as bankId','bank.name as bankName');


//        $qb->where('e.fileUpload = :fileUpload')->setParameter('fileUpload', $uploadID);
        $qb->where('fileUploadData.accountType = :accountType')->setParameter('accountType', $type);
        $qb->andWhere('fileUploadData.transactionDate = :transactionDate')->setParameter('transactionDate', $date);
        $qb->andWhere('e.bankTransaction IS NOT NULL');
        $qb->orderBy('e.reconciliationFlag');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }


    public function getDailyReconciliationAllBank($transactionDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');

        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug as bankSlug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('fileUpload.accountType');

        $qb->where('fileUpload.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);
        $qb->andWhere('e.approvedBy IS NOT NULL');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function ReconciliationDataCompanyBankWise($startDate,$endDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');

        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.reportDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');
        $qb->addSelect('fileUpload.accountType');

        /*$qb->where('fileUpload.transactionDate BETWEEN :startDate AND :endDate')
                        ->setParameter('startDate', $startDate)
                        ->setParameter('endDate', $endDate);*/

        $qb->where('bankTransaction.reportDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);

        $qb->andWhere('e.approvedBy IS NOT NULL');
        $qb->orderBy('bank.name','ASC');
        $records = $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($records as $record) {
            $data[$record['bankName']][$record['accountType']][] = $record;
        }
        return $data;
    }

    public function ReconciliationDataBankWise($startDate,$endDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');

        $qb->select('e.id as reconciliationID');
        $qb->addSelect('SUM(bankTransaction.deposit) as bankDeposit','bankTransaction.reportDate as transactionDate');
        $qb->addSelect('bank.name as bankName');

        /*$qb->where('fileUpload.transactionDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);*/
        $qb->where('bankTransaction.reportDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);

        $qb->andWhere('e.approvedBy IS NOT NULL');
//        $qb->groupBy('fileUpload.transactionDate','bank.id');
        $qb->groupBy('bankTransaction.reportDate','bank.id');
        $qb->orderBy('bank.name','ASC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function ReconciliationDataCompanyWise($startDate,$endDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');

        $qb->select('e.id as reconciliationID');
        $qb->addSelect('SUM(bankTransaction.deposit) as bankDeposit','bankTransaction.reportDate as transactionDate');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('fileUpload.accountType');

        /*$qb->where('fileUpload.transactionDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);*/
        $qb->where('bankTransaction.reportDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);

        $qb->andWhere('e.approvedBy IS NOT NULL');
        $qb->groupBy('fileUpload.accountType','bank.id','bankTransaction.reportDate');
        $qb->orderBy('bank.name','ASC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('e.fileUpload', 'fileUpload');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');

        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');
        $qb->addSelect('fileUpload.accountType as accountType');

        $qb->where('fileUpload.transactionDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);
        $qb->andWhere('e.approvedBy IS NOT NULL');
        $qb->andWhere('e.isCustom = :isCustom')->setParameter('isCustom',$isCustom);
        if (isset($accountType)){
            $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType',$accountType);
        }

        $qb->orderBy('bank.name','ASC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function UnrecognisedBankTransactonBranchAgent($branch,$limit){
//        dd($limit);
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('branch.bank', 'bank');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('agent.district', 'district');
        $qb->leftJoin('agent.upozila', 'upozila');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->join('e.bankTransaction', 'bankTransaction');


//        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag','e.createdAt');
        $qb->select('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bank.name as bankName');
        $qb->addSelect('bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('district.name AS districtName');
        $qb->addSelect('upozila.name AS upozilaName');
        $qb->addSelect('branch.branchName AS branchName');

        $qb->andWhere('e.branch = :branch')->setParameter('branch',$branch);
        $qb->andWhere('e.approvedBy IS NOT NULL');
        $qb->setMaxResults($limit);
//        $qb->orderBy('agent.name','ASC');
        $qb->orderBy('bankTransaction.transactionDate','DESC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }



    public function getDailyMissingDataByUploadID($bank,$transactionDateCheck,$type){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('e.fileUpload', 'fileUpload');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');
        $qb->leftJoin('BankAccountCode.bank', 'bank');


        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag','e.approvedAt','e.isDownload');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');

        $qb->where("((e.bankTransaction IS NOT NULL) AND
                      (
                          (e.approvedAt > :approvedAt) OR
                          (e.approvedBy IS NULL)
                      )
                 )"
            );
        $qb->setParameters([
//                'fileUpload' => $uploadID,
                'approvedAt' => $transactionDateCheck.' 12:00',
            ]);
        $qb->andWhere('bankTransaction.reportDate = :reportDate')->setParameter('reportDate',$transactionDateCheck);
        $qb->andWhere('bankTransaction.bank = :bank')->setParameter('bank',$bank);
        $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType',$type);
        $qb->orderBy('agent.name','DESC');
        $qb->orderBy('e.isDownload','DESC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function getDailyMissingDataByUploadIDOnlyAccount($bank,$transactionDateCheck,$type){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bankTransaction', 'bankTransaction');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftjoin('e.salesPayment', 'salesPayment');
        $qb->leftjoin('bankTransaction.branch', 'transactionBranch');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('e.fileUpload', 'fileUpload');
        $qb->leftJoin('fileUpload.BankAccountCode', 'BankAccountCode');
        $qb->leftJoin('BankAccountCode.bank', 'bank');


        $qb->select('e.id as reconciliationID','e.diffAmount','e.actualAmount','e.salesPaymentAmount','e.isCustom','e.mode','e.reconciliationFlag','e.approvedAt','e.isDownload');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');
        $qb->addSelect('bankTransaction.id as transactionID','bankTransaction.deposit as bankDeposit','bankTransaction.description','bankTransaction.transactionDate as transactionDate');
        $qb->addSelect('salesPayment.id AS salesID');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->addSelect('approvedBy.id AS approvedID','approvedBy.name as userName','approvedBy.mobile as userMobile');
        $qb->addSelect('transactionBranch.branchCode','transactionBranch.id as branchId','transactionBranch.branchName as branchName');
        $qb->addSelect('BankAccountCode.accountCode as accountCode','BankAccountCode.accountNo as accountNo');

        $qb->where("((e.bankTransaction IS NOT NULL) AND
                      (
                          (e.approvedAt > :approvedAt) OR
                          (e.approvedBy IS NULL)
                      )
                 )"
        );
        $qb->setParameters([
//            'fileUpload' => $uploadID,
            'approvedAt' => $transactionDateCheck.' 12:00',
        ]);
        $qb->andWhere('e.isDownload = :isDownload')->setParameter('isDownload',0);
        $qb->andWhere('bankTransaction.reportDate = :reportDate')->setParameter('reportDate',$transactionDateCheck);
        $qb->andWhere('bankTransaction.bank = :bank')->setParameter('bank',$bank);
        $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType',$type);
        $qb->andWhere('e.approvedBy IS NOT NULL');

        $qb->orderBy('agent.name','DESC');
        $qb->orderBy('e.isDownload','DESC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

}