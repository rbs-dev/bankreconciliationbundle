<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\SubFileUpload;


class BankTransactionRepository extends EntityRepository
{

    public function insertData(FileUpload $file, $fileDir)
    {
        set_time_limit(0);
        $em = $this->_em;
        $reader = new Xlsx();
        $spreadSheet = $reader->load($fileDir . $file->getFileName());
        $excelSheet = $spreadSheet->getActiveSheet();
        $allData = $excelSheet->toArray();

        $keys = array_shift($allData); //remove Excel column heading
        $totalHeading = count($keys);

        $keys = array_map('trim', array_filter($keys)); //remove all spaces from string
//        dd($file->getBank()->getSlug());

        if ($file->getBank()->getSlug() === 'united-commercial-bank'){
            if (!empty($allData)){
//                dd($allData,$totalHeading);
                if ($totalHeading == 3){
                    $this->ucblBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if($file->getBank()->getSlug() === 'ncc-bank'){
            if (!empty($allData)){
                if ($totalHeading == 5){
                    $this->nccBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'national-bank'){
            if (!empty($allData)){
//                dd($allData,$totalHeading);
                if ($totalHeading == 3){
                    $this->nationalBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'bangladesh-commerce-bank'){
            if (!empty($allData)){
//                dd($allData,$totalHeading);
                if ($totalHeading == 3) {
                    $this->commerceBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'bangladesh-krishi-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3){
                    $this->krishiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()==='union-bank'){
            if (!empty($allData)){
//                dd($allData,$totalHeading);
                if ($totalHeading == 3) {
                    $this->unionBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()==='Uttara-bank-ltd'){
            if (!empty($allData)){
//                correction format
                if ($totalHeading == 3) {
                    $this->uttaraBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
//                Old format
//                if ($totalHeading == 6) {
//                    $this->uttaraBank($em, $keys, $allData, $file);
//                    $file->setStatus(1);
//                    $em->persist($file);
//                    $em->flush();
//                    return true;
//                }else{
//                    return false;
//                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()=== 'bank-asia'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->asiaBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'sonali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->sonaliBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'janata-bank'){
            if (!empty($allData)){
//                correction format
                if ($totalHeading == 3) {
                    $this->janataBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
//                old format
//                if ($totalHeading == 6) {
//                    $this->janataBank($em, $keys, $allData, $file);
//                    $file->setStatus(1);
//                    $em->persist($file);
//                    $em->flush();
//                    return true;
//                }else{
//                    return false;
//                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'rupali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->rupaliBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'ific-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->ificBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        if ($file->getBank()->getSlug() === 'agrani-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->agraniBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() ==='mercantile-bank'){
            if (!empty($allData)){
                if ($totalHeading == 4) {
                    $this->mercantileBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'first-security-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->firstSecurityIslamiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'social-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->socialislamiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if (($file->getBank()->getSlug() === 'prime-bank-ltd') || ($file->getBank()->getSlug() === 'prime-bank')){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->primeBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'shahjalal-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->shahjalalislamiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'exim-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->eximBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'brac-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->bracBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'pubali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 6) {
                    $this->pubaliBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'islami-bank-bangladesh'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->islamiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'al-arafah-islami-bank'){
            $rowCount= sizeof($allData);
            if ($rowCount>0){
                if ($totalHeading == 3) {
                    $this->alarafahislamiBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'dutch-bangla-bank'){
            if (!empty($allData)){
                if ($totalHeading == 4) {
                    $this->dbblBank($em, $keys, $allData, $file);
                    $file->setStatus(1);
                    $em->persist($file);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
    public function insertDataSubfile(SubFileUpload $subFile, $fileDir,$file)
    {
        set_time_limit(0);
        $em = $this->_em;
        $reader = new Xlsx();
        $spreadSheet = $reader->load($fileDir . $subFile->getSubFileName());
        $excelSheet = $spreadSheet->getActiveSheet();
        $allData = $excelSheet->toArray();

        $keys = array_shift($allData); //remove Excel column heading
        $totalHeading = count($keys);

        $keys = array_map('trim', array_filter($keys)); //remove all spaces from string

        if ($file->getBank()->getSlug() === 'united-commercial-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3){
                    $this->ucblBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if($file->getBank()->getSlug() === 'ncc-bank'){
            if (!empty($allData)){
                if ($totalHeading == 5){
                    $this->nccBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'national-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3){
                    $this->nationalBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'bangladesh-commerce-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->commerceBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'bangladesh-krishi-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3){
                    $this->krishiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()==='union-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->unionBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()==='Uttara-bank-ltd'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->uttaraBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ( $file->getBank()->getSlug()=== 'bank-asia'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->asiaBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'sonali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->sonaliBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'janata-bank'){
            if (!empty($allData)){
//                correction format
                if ($totalHeading == 3) {
                    $this->janataBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'rupali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->rupaliBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'ific-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->ificBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        if ($file->getBank()->getSlug() === 'agrani-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->agraniBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() ==='mercantile-bank'){
            if (!empty($allData)){
                if ($totalHeading == 4) {
                    $this->mercantileBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'first-security-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->firstSecurityIslamiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'social-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->socialislamiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if (($file->getBank()->getSlug() === 'prime-bank-ltd') || ($file->getBank()->getSlug() === 'prime-bank')){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->primeBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'shahjalal-islami-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->shahjalalislamiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'exim-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->eximBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'brac-bank'){
            if (!empty($allData)){
                if ($totalHeading == 3) {
                    $this->bracBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'pubali-bank'){
            if (!empty($allData)){
                if ($totalHeading == 6) {
                    $this->pubaliBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'islami-bank-bangladesh'){
            if (!empty($allData)){
                if ($totalHeading == 7) {
                    $this->islamiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'al-arafah-islami-bank'){
            $rowCount= sizeof($allData);
            if ($rowCount>0){
                if ($totalHeading == 3) {
                    $this->alarafahislamiBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        if ($file->getBank()->getSlug() === 'dutch-bangla-bank'){
            if (!empty($allData)){
                if ($totalHeading == 4) {
                    $this->dbblBank($em, $keys, $allData, $file);
                    $subFile->setStatus(1);
                    $em->persist($subFile);
                    $em->flush();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

    public function ucblBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y',$TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function nccBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] !='Balance B/F') {
                if ($values[0]) {
                    $date = date_create_from_format('d/m/Y', $values[0]);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[1]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[3])));
                        $transaction->setBalance((double)trim(str_replace(',', '', $values[4])));
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function nationalBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function commerceBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));

            if ($values[1] != 'Balance Brought Forward' && $values[0] != null ) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function krishiBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function unionBank($em, $keys, $allData, FileUpload $file)
    {
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function uttaraBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0] != 'Opening Balance') {
                if ($values[0]) {
                    $date = date_create_from_format('d/m/Y', $values[0]);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[1]));
                        $transaction->settransactionRef(trim($values[1]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function asiaBank($em, $keys, $allData, FileUpload $file)
    {
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function sonaliBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[3] != 'Opening Balance') {
                if ($values[0]) {
                    $TransactionDate = date('d/m/Y', strtotime($values[0]));
                    $date = date_create_from_format('d/m/Y', $TransactionDate);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[2]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[5])));
                        $balance = trim(str_replace(',', '', $values[6]));
                        $balance = strtok($balance, ' ');
                        $transaction->setBalance((double)$balance);
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function janataBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function rupaliBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[3] != 'Opening Balance') {
                if ($values[0]) {
                    $TransactionDate = date('d/m/Y', strtotime($values[0]));
                    $date = date_create_from_format('d/m/Y', $TransactionDate);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[2]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[5])));
                        $balance = trim(str_replace(',', '', $values[6]));
                        $balance = strtok($balance, ' ');
                        $transaction->setBalance((double)$balance);
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function ificBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[1]) {
                $TransactionDate = date('d/m/Y', strtotime($values[1]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[0]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function agraniBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function mercantileBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');

                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[2]));
                    $transaction->settransactionRef(trim($values[2]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[3])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function firstSecurityIslamiBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] != 'Opening Balance' && $values[0] != null) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }


    public function socialislamiBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] != 'Balance B/F' && $values[0] != null && $values[2] != 0) {
                $date = date_create_from_format('d-m-Y', $values[0]);
                if (!$date){
                    $date = date_create_from_format('d/m/Y', $values[0]);
                }
                $transDate = $date->format('Y-m-d');
                if ($transDate == $transactionDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function primeBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] != 'BALANCE AT PERIOD START :' && $values != null) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }


    public function shahjalalislamiBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[0] != 'Opening Balance') {
                if($values[0]) {
                    $date = date_create_from_format('d/m/Y', $values[0]);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[1]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function eximBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->settransactionRef(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function bracBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[2] != 'Balance Forward' && $values[0]!=null) {
                $date = date_create_from_format('d/m/Y', $values[0]);
                $transDate = $date->format('Y-m-d');
                if ( $transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function pubaliBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] != 'OPENING BALANCE') {
                if($values[0]) {
                    $date = date_create_from_format('d/m/Y', $values[0]);
                    $transDate = $date->format('Y-m-d');
                    if ($transactionDate == $transDate) {
                        $transaction->setTransactionDate(new \DateTime($transDate));
                        $transaction->setReportDate(new \DateTime($transDate));
                        $transaction->setDescription(trim($values[1]));
                        $transaction->setDeposit((double)trim(str_replace(',', '', $values[4])));
                        $transaction->setBalance((double)trim(str_replace(',', '', $values[5])));
                        $transaction->setBank($file->getBank());
                        $transaction->setFileUpload($file);
                        $transaction->setCreatedAt(new \DateTime('now'));
                        $em->persist($transaction);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function dbblBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[0]) {
                $TransactionDate = date('d/m/Y', strtotime($values[0]));
                $date1 = date_create_from_format('d/m/Y', $TransactionDate);
                $transDate = $date1->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setInstrumentNo(trim($values[2] . '#' . $values[2]));
                    $transaction->settransactionRef(trim($values[2]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[3])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }

    public function alarafahislamiBank($em, $keys, $allData, FileUpload $file){
        $transactionDate = $file->getTransactionDate()->format('Y-m-d');
        foreach ($allData as $data) {
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if ($values[1] != 'Balance B/F' && $values[0] !=null) {
                $date = date_create_from_format('d-m-Y', $values[0]);
                if (!$date){
                    $date = date_create_from_format('d/m/Y', $values[0]);
                }

                $transDate = $date->format('Y-m-d');
                if ($transactionDate == $transDate) {
                    $transaction->setTransactionDate(new \DateTime($transDate));
                    $transaction->setReportDate(new \DateTime($transDate));
                    $transaction->setDescription(trim($values[1]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[2])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }


    private function islamiBank($em, $keys, $allData, FileUpload $file)
    {
        $transactionDate = $file->getTransactionDate()->format('d-m-Y');
        foreach ($allData as $data){
            $transaction = new BankTransaction();
            $values = array_slice($data, null, count($keys));
            if($values[0]) {
                $transDate = \DateTime::createFromFormat('d/m/y', $values[0]);
                if ($transactionDate == $transDate->format('d-m-Y')) {
                    $transaction->setTransactionDate($transDate);
                    $transaction->setReportDate($transDate);
                    $postDate = \DateTime::createFromFormat('d/m/y', $values[1]);
                    $transaction->setPostDate($postDate);
                    $transaction->setDescription(trim($values[2]));
                    $transaction->setInstrumentNo(trim($values[3]));
                    $transaction->setDeposit((double)trim(str_replace(',', '', $values[5])));
                    $transaction->setBalance((double)trim(str_replace(',', '', $values[6])));
                    $transaction->setBank($file->getBank());
                    $transaction->setFileUpload($file);
                    $transaction->setCreatedAt(new \DateTime('now'));
                    $em->persist($transaction);
                    $em->flush();
                }
            }
        }
    }
    
    public function getTransactions($bank, $transactionDate)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.branch', 'branch');
        $qb->join('branch.bank', 'bank');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->select('e.id AS statementId','e.description', 'e.deposit', 'e.transactionDate', 'approvedBy.userId');
        $qb->addSelect('branch.branchName', 'branch.branchCode');
        $qb->where('e.status = 1');
        $qb->andWhere('bank.id = :bankId')->setParameter('bankId', $bank->getId());
        $qb->andWhere('e.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);

        $records = $qb->getQuery()->getArrayResult();
        $data = [];
        foreach ($records as $record) {
            $data[$record['branchCode']. '-' .$record['deposit']] = $record;
        }
        return $data;
    }

    public function UnrecognisedBankTransacton($startDate,$endDate,$accountType,$bank){
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.branch', 'branch');
        $qb->leftJoin('e.fileUpload', 'fileUpload');
        $qb->leftJoin('branch.district', 'district');
        $qb->leftJoin('e.bank', 'bank');
        $qb->leftJoin('bank.accountCode', 'accountCode');
        $qb->join('e.bankReconciliation', 'bankReconciliation');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->select('e.id AS transactionId','e.description', 'e.deposit', 'e.transactionDate', 'approvedBy.userId');
        $qb->addSelect('branch.branchName', 'branch.branchCode');
        $qb->addSelect('bank.name as BankName');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('accountCode.accountCode');
        $qb->addSelect('fileUpload.accountType');
//        $qb->where('e.status = 1');
        $qb->where('bankReconciliation.approvedBy IS NULL');
        $qb->andWhere('accountCode.accountType = :accountCode')->setParameter('accountCode',$accountType);
        $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType',$accountType);

        $qb->andWhere('e.transactionDate BETWEEN :startDate AND :endDate')
                        ->setParameter('startDate', $startDate)
                        ->setParameter('endDate', $endDate);
        if ($bank != null){
            $qb->andWhere('fileUpload.bank = :bank')->setParameter('bank',$bank);
        }

        $records = $qb->getQuery()->getArrayResult();
//        $data = [];
//        foreach ($records as $record) {
//            $data[$record['branchCode']. '-' .$record['deposit']] = $record;
//        }
        return $records;
    }
}