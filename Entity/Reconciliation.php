<?php

namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\ReconciliationRepository")
 * @ORM\Table(name="br_reconciliation")
 */

class Reconciliation
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="boolean")
     */
    protected $status = false;

    /**
     * @var integer
     * @ORM\Column(type="boolean")
     */
    protected $isDownload = false;

    /**
     * @var BankTransaction
     * @ORM\OneToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankTransaction", inversedBy="bankReconciliation", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $bankTransaction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $approvedBy;
    
    /**
     * @var $fileUpload
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\FileUpload",inversedBy="reconciliations")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $fileUpload;

    /**
     * @var SalesPayment
     * @ORM\OneToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\SalesPayment",cascade={"persist"},inversedBy="bankReconciliation")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $salesPayment;
    
    /**
     * @var BankBranch
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankBranch", inversedBy="reconciliation")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $branch;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Agent")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $agent;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true)
     */
    private $isCustom = 'no';

    /**
     * @var double
     * @ORM\Column(type="float", nullable=true)
     */
    private $transactionAmount;

    /**
     * @var string
     * @ORM\Column(type="text", nullable = true)
     */
    protected $mode = 'Feed';
    
    /**
     * @var double
     * @ORM\Column(type="float", nullable=true)
     */
    private $salesPaymentAmount;

     /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $reconciliationFlag;

    /**
     * @var double
     * @ORM\Column(type="float", nullable=true)
     */
    private $actualAmount;

    /**
     * @var double
     * @ORM\Column(type="float", nullable=true)
     */
    private $diffAmount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $approvedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return BankTransaction
     */
    public function getBankTransaction()
    {
        return $this->bankTransaction;
    }

    /**
     * @param BankTransaction $bankTransaction
     */
    public function setBankTransaction(BankTransaction $bankTransaction): void
    {
        $this->bankTransaction = $bankTransaction;
    }

    /**
     * @return SalesPayment
     */
    public function getSalesPayment()
    {
        return $this->salesPayment;
    }

    public function setSalesPayment( $salesPayment)
    {
        $this->salesPayment = $salesPayment;
    }

    /**
     * @return BankBranch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param BankBranch $branch
     */
    public function setBranch(BankBranch $branch)
    {
        $this->branch = $branch;
    }


    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @return float
     */
    public function getTransactionAmount(): float
    {
        return $this->transactionAmount;
    }

    /**
     * @param float $transactionAmount
     */
    public function setTransactionAmount(float $transactionAmount): void
    {
        $this->transactionAmount = $transactionAmount;
    }

    /**
     * @return float
     */
    public function getSalesPaymentAmount()
    {
        return $this->salesPaymentAmount;
    }

    /**
     * @param $salesPaymentAmount
     */
    public function setSalesPaymentAmount( $salesPaymentAmount): void
    {
        $this->salesPaymentAmount = $salesPaymentAmount;
    }

    /**
     * @return float
     */
    public function getDiffAmount()
    {
        return $this->diffAmount;
    }


    public function setDiffAmount( $diffAmount)
    {
        $this->diffAmount = $diffAmount;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return FileUpload
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @param FileUpload $fileUpload
     */
    public function setFileUpload($fileUpload): void
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @return float
     */
    public function getActualAmount()
    {
        return $this->actualAmount;
    }

    /**
     * @param float $actualAmount
     */
    public function setActualAmount( $actualAmount)
    {
        $this->actualAmount = $actualAmount;
    }

    /**
     * @return string
     */
    public function getReconciliationFlag(): string
    {
        return $this->reconciliationFlag;
    }

    /**
     * @param string $reconciliationFlag
     */
    public function setReconciliationFlag(string $reconciliationFlag): void
    {
        $this->reconciliationFlag = $reconciliationFlag;
    }


    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param mixed $approvedBy
     */
    public function setApprovedBy($approvedBy): void
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getIsCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param string $isCustom
     */
    public function setIsCustom(string $isCustom): void
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return \DateTime
     */
    public function getApprovedAt(): \DateTime
    {
        return $this->approvedAt;
    }

    /**
     * @param \DateTime $approvedAt
     */
    public function setApprovedAt(\DateTime $approvedAt): void
    {
        $this->approvedAt = $approvedAt;
    }

    /**
     * @return int
     */
    public function getIsDownload(): int
    {
        return $this->isDownload;
    }

    /**
     * @param int $isDownload
     */
    public function setIsDownload(int $isDownload): void
    {
        $this->isDownload = $isDownload;
    }
}