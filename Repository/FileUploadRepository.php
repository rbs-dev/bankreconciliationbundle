<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;


class FileUploadRepository extends EntityRepository
{
    public function uploadData(){
        $qb = $this->createQueryBuilder('e');
        $qb->innerJoin('e.transactions', 'transactions');
//        $qb->leftJoin('e.bank', 'bank');
//        $qb->leftJoin('e.reconciliations', 'reconciliations');
        $qb->select('e.id','e.type','e.accountType','e.fileName','e.status','e.transactionDate');
//        $qb->addSelect('bank.name','bank.id');
        $qb->addSelect('sum(transactions.deposit) as depositeTotal');

        $qb->where('e.type = :type')->setParameter('type', 'bank-statement');
        $qb->orderBy('e.id','DESC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }
    public function allUploadFileWithTotal(){
        $qb = $this->createQueryBuilder('e');
//        $qb->leftJoin('e.reconciliations','reconciliations');
        $qb->join('e.bank','bank');
        $qb->join('e.transactions','transactions');

        $qb->select('e.id','e.type','e.accountType','e.fileName','e.status','e.transactionDate','e.createdAt');
//        $qb->addSelect('sum(reconciliations.transactionAmount) as depositeTotal');
        $qb->addSelect('sum(transactions.deposit) as depositeTotal');
        $qb->addSelect('bank.name as bankName');

        $qb->where('e.type = :type')->setParameter('type', 'bank-statement');
        $qb->orderBy('e.id','DESC');
//        $qb->groupBy('reconciliations.fileUpload');
        $qb->groupBy('transactions.fileUpload');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function totalBankTransacton($startDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank','bank');
        $qb->join('e.transactions','transactions');
        $qb->join('transactions.bankReconciliation','bankReconciliation');

        $qb->select('e.id','e.accountType','e.transactionDate','e.createdAt');
        $qb->addSelect('sum(transactions.deposit) as depositeTotal');
        $qb->addSelect('bank.name as bankName');
        $qb->addSelect('bankReconciliation.mode');

        $qb->where('e.transactionDate = :transactionDate')->setParameter('transactionDate', $startDate);
//        $qb->andWhere('transactions.reportDate = :reportDate')->setParameter('reportDate', $startDate);
        $qb->andWhere('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('bank.name','ASC');
        $qb->groupBy('e.bank','e.accountType','bankReconciliation.mode');
        $records = $qb->getQuery()->getArrayResult();
//        dd($records);

        $resultArray = [];
        if ($records){
            foreach ($records as $record){
                $resultArray[$record['bankName']][$record['accountType']][$record['mode']] = $record['depositeTotal'];
            }
        }
//        dd($resultArray);
        return $resultArray;

        /*$resultArray = [];
        if ($records){
            foreach ($records as $record){
                $resultArray[$record['bankName']][$record['accountType']] = $record['depositeTotal'];
            }
        }
        return $resultArray;*/
    }

    public function transactionDownload($file){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.transactions','transactions');

        $qb->select('e.id','e.accountType','e.transactionDate','e.createdAt');
        $qb->addSelect('transactions.deposit','transactions.description','transactions.transactionRef');

        $qb->where('e.id = :id')->setParameter('id', $file);
        $qb->andWhere('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('e.id','ASC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }
}