$('.agentSearch').select2({
    ajax: {
        url: Routing.generate('core_agent_search_select2'),
        dataType: 'json',
        delay: 250,
        type: 'GET',
        data: function (params) {

            return {
                q: params.term, // search term
            };
        },
        processResults: function (data) {
            var arr = [];
            $.each(data, function (index, value) {
                arr.push({
                    id: index,
                    text: value
                })
            })
            return {
                results: arr
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1
});