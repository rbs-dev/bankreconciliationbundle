<?php

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use App\Entity\SalesDepot;
use App\Entity\User;
use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\SmsConvert;

class SmsConvertFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $userDepotID = $options['user']->getDepot()->getId();
        $transactionDate = $builder->getData()->getTransactionDate();
        $builder
            /*->add('depotId', EntityType::class,[
                'class' => SalesDepot::class,
                'placeholder' => 'Choose Depot',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2',
                    'choice_value' => $userDepotID
                ]
            ])*/
            /*->add('depotId', TextType::class,[
                'required' => true,
            ])*/

            ->add('agentId',ChoiceType::class,[
                'required' => true,
                'placeholder' => 'Choose Agent',
                'attr' => [
                    'class' => 'select2 agentSearch'
                ],
                'mapped' => false,
                'multiple' => false
            ])
            ->add('bank', EntityType::class,[
                'class' => Bank::class,
                'placeholder' => 'Choose Bank',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where('e.status = 1')
                    ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])

            ->add('branchId',ChoiceType::class,[
                'required' => true,
                'placeholder' => 'Choose Branch',
                'attr' => [
                    'class' => ' branchSearch'
                ],
                'mapped' => false,
                'multiple' => false,
            ])

            ->add('type',ChoiceType::class,[
                'choices' => [
                    'Feed' => 'Feed',
                    'Chick' => 'Chick'
                ],
                'placeholder' => 'Choose Type',
                'attr' => [
                    'class' => 'agentType'
                ]
            ])
            ->add('amount', TextType::class,[
                'required' => true,
            ])

            ->add('transactionDate', \Symfony\Component\Form\Extension\Core\Type\DateType::class,[
                'required' => true,
                'widget' => 'single_text',
                'data' => $transactionDate?$transactionDate:new \DateTime()
            ])

            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled",
                    'checked' => "checked"
                ],
            ])
            ->add('submit', SubmitType::class)
            ->setMethod('post')
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SmsConvert::class,
            'user' => User::class,
        ]);
    }



}