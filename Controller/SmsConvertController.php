<?php


namespace Terminalbd\BankReconciliationBundle\Controller;

use App\Entity\Admin\Bank;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use App\Entity\SalesDepot;
use Dompdf\Dompdf;
use Dompdf\Options;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;
use Terminalbd\BankReconciliationBundle\Entity\SmsConvert;
use Terminalbd\BankReconciliationBundle\Form\BankAccontCodeFormType;
use Terminalbd\BankReconciliationBundle\Form\DateRangeFormType;
use Terminalbd\BankReconciliationBundle\Form\SmsConvertFormType;
use Terminalbd\BankReconciliationBundle\Form\SmsConvertSearchFormType;


/**
 * Class BankBranchController
 * @package Terminalbd\BankReconciliationBundle\Controller
 */

class SmsConvertController extends AbstractController
{

    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/sms/convert/list", name="br_sms_convert_list")
     */
    public function index(Request $request)
    {
        $records = $this->getDoctrine()->getRepository(SmsConvert::class)->GetAllSmsConvert();
        $data = $this->paginate($request, $records);
        return $this->render('@TerminalbdBankReconciliation/sms-convert/index.html.twig',[
            'data' => $data
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER')")
     * @param Request $request
     * @Route("/sms/convert/create", name="br_sms_convert_create")
     */
    public function convertData(Request $request)
    {
        $smsConvert = new SmsConvert();
        $user = $this->getUser();

        $form = $this->createForm(SmsConvertFormType::class,$smsConvert);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if ($user->getDepot()) {
                $em = $this->getDoctrine()->getManager();
                $bank = $form['bank']->getData();
//            $depot = $form['depotId']->getData();
                $type = $form['type']->getData();
                $amount = $form['amount']->getData();
                $transactionDate = $form['transactionDate']->getData();
                $status = $form['status']->getData();
                $data = $request->request->all();
                $agentId = $data['sms_convert_form']['agentId'];
                $branchId = $data['sms_convert_form']['branchId'];

                $agent = $this->getDoctrine()->getRepository(Agent::class)->find($agentId);
                $branch = $this->getDoctrine()->getRepository(BankBranch::class)->find($branchId);

                if ($agent && $branch) {
                    $smsConvert->setAgent($agent);
                    $smsConvert->setBank($bank);
                    $smsConvert->setBranch($branch);
                    $smsConvert->setType($type);
                    $smsConvert->setDepotId($user->getDepot());
                    $smsConvert->setAmount($amount);
                    $smsConvert->setTransactionDate($transactionDate);
                    $smsConvert->setStatus($status);
                    $smsConvert->setCreatedBy($this->getUser());
                    $em->persist($smsConvert);
                    $em->flush();

                    $this->addFlash('success', 'SMS Convert added Successfully !');
                    return $this->redirectToRoute('br_sms_convert_list');
                } else {
                    $this->addFlash('success', 'Something went wrong !');
                    return $this->redirect($request->server->get('HTTP_REFERER'));
                }
            }else{
                $this->addFlash('warning', 'Depot missing , assign depot !');
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        return $this->render('@TerminalbdBankReconciliation/sms-convert/create.html.twig', [
            'form' => $form->createView(),
            'entity'=>null
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER')")
     * @param Request $request
     * @Route("/sms/convert/{id}/update", name="br_sms_convert_update")
     */
    public function smsConvertUpdate(Request $request,SmsConvert $smsConvert){
        $form = $this->createForm(SmsConvertFormType::class, $smsConvert);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $bank = $form['bank']->getData();
            $depot = $form['depotId']->getData();
            $type = $form['type']->getData();
            $amount = $form['amount']->getData();
            $transactionDate = $form['transactionDate']->getData();
            $status = $form['status']->getData();
            $data = $request->request->all();
            $agentId = $data['sms_convert_form']['agentId'];
            $branchId = $data['sms_convert_form']['branchId'];
            $agent = $this->getDoctrine()->getRepository(Agent::class)->find($agentId);
            $branch = $this->getDoctrine()->getRepository(BankBranch::class)->find($branchId);

            if ($agent && $branch){
                $smsConvert->setAgent($agent);
                $smsConvert->setBank($bank);
                $smsConvert->setBranch($branch);
                $smsConvert->setType($type);
                $smsConvert->setAmount($amount);
                $smsConvert->setTransactionDate($transactionDate);
                $smsConvert->setStatus($status);
                $smsConvert->setDepotId($depot);
                $smsConvert->setUpdatedBy($this->getUser());
                $em->persist($smsConvert);
                $em->flush();

                $this->addFlash('success', 'SMS Convert update Successfully !');
                return $this->redirectToRoute('br_sms_convert_list');
            }else{
                $this->addFlash('success', 'Something went wrong !');
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }
        return $this->render('@TerminalbdBankReconciliation/sms-convert/create.html.twig',[
            'form' => $form->createView(),
            'entity' => $smsConvert
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sms/convert/{id}/delete", name="br_sms_convert_delete")
     */

    public function smsConvertDelete(SmsConvert $smsConvert){
        $em = $this->getDoctrine()->getManager();
        $em->remove($smsConvert);
        $em->flush();

        $this->addFlash('success', 'SMS Convert has been deleted!');
        return $this->redirectToRoute('br_sms_convert_list');
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sms/convert/{id}/update/reconciliation/{type}", name="br_sms_convert_update_reconciliation")
     */

    public function smsConvertUpdateReconciliation(SmsConvert $smsConvert,$type){
        $em = $this->getDoctrine()->getManager();
        if($type == 1){
            $smsConvert->setApprovedBy($this->getUser());
        }else{
            $smsConvert->setApprovedBy(null);
        }
        $smsConvert->setIsReconciliation($type);
        $em->persist($smsConvert);
        $em->flush();

        if ($type == 1){
            $message = 'SMS convert reconciliation complete';
        }else{
            $message = 'SMS convert reconciliation rollback';
        }
        $this->addFlash('success', $message);
        return $this->redirectToRoute('br_sms_convert_list');
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sms/convert/recognised/list", name="br_sms_convert_recognised_list")
     */

    public function smsConvertRecognisedList(Request $request)
    {
        $form = $this->createForm(SmsConvertSearchFormType::class);
        $form->handleRequest($request);
        $records = [];
        $transactionDate = '';
        $type = '';
        $bank = '';
        $isReconciliation = '';
        $depot = '';
        if ($form->isSubmitted()){
            $transactionDate = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($transactionDate));
            $transactionDate = new \DateTime($transactionDate);

            $type = $form['type']->getData();
            $bank = $form['bank']->getData();
            $depot = $form['depotId']->getData();
            $isReconciliation = $form['isReconciliation']->getData();

            $records = $this->getDoctrine()->getRepository(SmsConvert::class)
                                            ->recognisedSmsConvert($isReconciliation,$transactionDate,$type,$bank,$depot);
            if (!empty($depot)){
                $depot = $depot->getId();
            }
        }


        return $this->render('@TerminalbdBankReconciliation/report/convert/recognised-convert.html.twig',[
            'form' => $form->createView(),
            'data' => $records,
            'transactionDate' => $transactionDate,
            'type' => $type,
            'bank' => $bank,
            'isReconciliation' => $isReconciliation,
            'depot' => $depot,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sms/convert/recognised/download", name="br_sms_convert_recognised_download")
     */
    public function smsConvertRecognisedDownload(Request $request)
    {
        $isReconciliation = $request->query->get('isReconciliation');
        $transactionDate = $request->query->get('transactionDate');
        $transactionDate = date("Y-m-d", strtotime($transactionDate));
        $transactionDate = new \DateTime($transactionDate);
        $type = $request->query->get('type');
        $bank = $request->query->get('bank');
        $filetype = $request->query->get('filetype');
        $depot = $request->query->get('depot');

        $records = $this->getDoctrine()->getRepository(SmsConvert::class)
            ->recognisedSmsConvert($isReconciliation,$transactionDate,$type,$bank,$depot);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/convert/recognised-convert-excel.html.twig',[
            'data' => $records,
            'transactionDate' => $transactionDate,
            'type' => $type,
            'bank' => $bank,
            'isReconciliation' => $isReconciliation,
        ]);

        if ($isReconciliation == 1){
            $reconciliation = 'Recognised';
        }else{
            $reconciliation = 'Unrecognised';
        }

        if ($filetype == 'pdf'){
            $date = $request->query->get('transactionDate');
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $dompdf = new Dompdf($pdfOptions);
            $dompdf->loadHtml($html);
            $dompdf->setPaper('legal', 'landscape');
            $dompdf->render();
            $fileName = $date.'_'.$reconciliation.'_sms_convert_'.".pdf";
            $dompdf->stream($fileName, [
                "Attachment" => true
            ]);
            die();
        }

        if ($filetype == 'excel'){
            $date = $request->query->get('transactionDate');
            $fileName = $date.'_'.$reconciliation.'_sms_convert_'.".xls";

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die;
        }
    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sms/convert/sunc/depot", name="br_sms_convert_sync_depot")
     */

    public function smsConvertSyncDepot(ParameterBagInterface $parameterBag, HttpClientInterface $client){
        $apiEndPoint = $parameterBag->get('salesDomain') . '/api/depots';
        $response = $client->request('GET', $apiEndPoint, [
            'headers' => [
                'X-API-KEY' => '79b8428a0dea686430a7f20ccbe857bd'
            ]
        ]);
        $records = $response->toArray();

        foreach ($records as $record) {
            $isExists = $this->getDoctrine()->getRepository(SalesDepot::class)->findOneBy(['depotId'=>$record['id']]);

            $em = $this->getDoctrine()->getManager();
            $status = false;
            if (empty($isExists)){
                $salesDepot = new SalesDepot();
                $salesDepot->setStatus(1);
                $salesDepot->setName($record['name']);
                $salesDepot->setType($record['type']);
                $salesDepot->setDepotId($record['id']);
                $em->persist($salesDepot);
                $em->flush();
                $status = true;
            }
        }
        if ($status){
            $message = 'Depot synchronisation complete';
        }else{
            $message = 'Nothing to synchronisation';
        }
        $this->addFlash('success', $message);
        return $this->redirectToRoute('br_sms_convert_create');
    }


    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE') or is_granted('ROLE_USER')")
     * @Route("/search/agent/for/sms", methods={"GET"}, name="core_agent_search_select2_for_sms_center", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function searchSelect2ForSMS(Request $request): Response
    {
        $data = $request->query->get('q');
        $agentType = $request->query->get('i');

        if ($agentType == 'Feed'){
            $groupID = 10;
        }

        if ($agentType == 'Chick'){
            $groupID = 11;
        }
        $modeObj = $this->getDoctrine()->getRepository(Setting::class)->find($groupID);
        $agents = $this->getDoctrine()->getRepository(Agent::class)->getAgentSelect2ForReconciliation($data,$modeObj);
        return new JsonResponse($agents);
    }
}