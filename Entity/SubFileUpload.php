<?php

namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\SubFileUploadRepository")
 * @ORM\Table(name="br_import_sub_file")
 */

class SubFileUpload
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    protected $status = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subFileName;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\FileUpload", inversedBy="subFile")
     * @ORM\JoinColumn(referencedColumnName="id",onDelete="cascade")
     */
    private $fileUpload;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $batchName;



    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;

    /**
     * @var $uploadedBy
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="fileUpload")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $uploadedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSubFileName()
    {
        return $this->subFileName;
    }

    /**
     * @param mixed $subFileName
     */
    public function setSubFileName($subFileName)
    {
        $this->subFileName = $subFileName;
    }

    /**
     * @return mixed
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @param mixed $fileUpload
     */
    public function setFileUpload($fileUpload): void
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @return mixed
     */
    public function getBatchName()
    {
        return $this->batchName;
    }

    /**
     * @param mixed $batchName
     */
    public function setBatchName($batchName)
    {
        $this->batchName = $batchName;
    }




    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUploadedBy()
    {
        return $this->uploadedBy;
    }

    /**
     * @param mixed $uploadedBy
     */
    public function setUploadedBy($uploadedBy): void
    {
        $this->uploadedBy = $uploadedBy;
    }

}