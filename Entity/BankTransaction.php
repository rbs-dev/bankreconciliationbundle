<?php

namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\BankTransactionRepository")
 * @ORM\Table(name="br_bank_transaction")
 */

class BankTransaction
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="boolean", nullable = true)
     */
    protected $status = 0;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank", inversedBy="transactions")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $bank;
    
    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankBranch", inversedBy="transactions")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $branch;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\Reconciliation", mappedBy="bankTransaction")
     */
    private $reconciliations;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $transactionRef;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $matchingPercentage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $balance;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $agentAmount;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $process;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $approvedBy;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\FileUpload", inversedBy="transactions")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="cascade")
     */
    private $fileUpload;

    /**
     * @var $bankReconciliation
     * @ORM\OneToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\Reconciliation", mappedBy="bankTransaction")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $bankReconciliation;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="date", nullable = true)
     */
    private $transactionDate;

    /**
     * @ORM\Column(type="date", nullable = true)
     */
    private $postDate;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $instrumentNo;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $deposit;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true)
     */
    private $isAdvance = 'no';

    /**
     * @ORM\Column(type="date", nullable = true)
     */
    private $reportDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIsAdvance()
    {
        return $this->isAdvance;
    }

    /**
     * @param string $isAdvance
     */
    public function setIsAdvance(string $isAdvance): void
    {
        $this->isAdvance = $isAdvance;
    }



    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     */
    public function setBank($bank): void
    {
        $this->bank = $bank;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch): void
    {
        $this->branch = $branch;
    }
    

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function gettransactionRef()
    {
        return $this->transactionRef;
    }

    /**
     * @param mixed $transactionRef
     */
    public function settransactionRef($transactionRef): void
    {
        $this->transactionRef = $transactionRef;
    }





    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getMatchingPercentage()
    {
        return $this->matchingPercentage;
    }

    /**
     * @param mixed $matchingPercentage
     */
    public function setMatchingPercentage($matchingPercentage): void
    {
        $this->matchingPercentage = $matchingPercentage;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getAgentAmount()
    {
        return $this->agentAmount;
    }

    /**
     * @param mixed $agentAmount
     */
    public function setAgentAmount($agentAmount): void
    {
        $this->agentAmount = $agentAmount;
    }

    /**
     * @return mixed
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param mixed $process
     */
    public function setProcess($process): void
    {
        $this->process = $process;
    }

    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param mixed $approvedBy
     */
    public function setApprovedBy($approvedBy): void
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return mixed
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @param mixed $fileUpload
     */
    public function setFileUpload($fileUpload): void
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @param mixed $transactionDate
     */
    public function setTransactionDate($transactionDate): void
    {
        $this->transactionDate = $transactionDate;
    }

    /**
     * @return mixed
     */
    public function getPostDate()
    {
        return $this->postDate;
    }

    /**
     * @param mixed $postDate
     */
    public function setPostDate($postDate): void
    {
        $this->postDate = $postDate;
    }


    /**
     * @return mixed
     */
    public function getInstrumentNo()
    {
        return $this->instrumentNo;
    }

    /**
     * @param mixed $instrumentNo
     */
    public function setInstrumentNo($instrumentNo): void
    {
        $this->instrumentNo = $instrumentNo;
    }

    /**
     * @return mixed
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * @param mixed $deposit
     */
    public function setDeposit($deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return mixed
     */
    public function getReconciliations()
    {
        return $this->reconciliations;
    }

    /**
     * @return mixed
     */
    public function getBankReconciliation()
    {
        return $this->bankReconciliation;
    }

    /**
     * @param mixed $bankReconciliation
     */
    public function setBankReconciliation($bankReconciliation): void
    {
        $this->bankReconciliation = $bankReconciliation;
    }

    /**
     * @return mixed
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * @param mixed $reportDate
     */
    public function setReportDate($reportDate): void
    {
        $this->reportDate = $reportDate;
    }









}