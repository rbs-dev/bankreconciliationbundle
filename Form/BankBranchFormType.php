<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;

class BankBranchFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('bank', EntityType::class,[
                'class' => Bank::class,
                'placeholder' => 'Select Bank',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where('e.status = 1')
                    ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('branchName', TextType::class,[
                'required' => true
            ])
            ->add('branchCode', TextType::class,[
                'required' => false
            ])
            ->add('district', EntityType::class,[
                'class' => Location::class,
                'placeholder' => 'Select District',
                'choice_label' => 'name',
                'required' => false,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1')
                        ->where('e.level = 4')
                        ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('mobile', TextType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'mobileLocal'
                ]
            ])
            ->add('bulkUploadFile', FileType::class,[
                'mapped' => false,
                'required' => false,
                'help' => 'Here you can upload only excel file with specific structure to upload branches  *Please check the image below'
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled",
                    'checked' => "checked"
                ],
            ])
            ->add('submit', SubmitType::class)
            ->setMethod('post')
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BankBranch::class,
//            'allow_extra_fields' => true,
        ]);
    }



}