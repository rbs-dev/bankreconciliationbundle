<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;

class SalesPaymentFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('depositAmount',TextType::class,[
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off'
                ],
            ])
            ->add('depositDate',DateType::class,[
                'required' => false,
                'widget' => 'single_text',
                'empty_data' => '',
            ])
//            ->add('salesId',TextType::class,[
//                'required' => false
//
//            ])



        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SalesPayment::class,
        ]);
    }


}