<?php


namespace Terminalbd\BankReconciliationBundle\Controller\Api;


use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Terminalbd\BankReconciliationBundle\Entity\Api\Api;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;
use Terminalbd\BankReconciliationBundle\Entity\ReconciliationRange;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;
use Terminalbd\BankReconciliationBundle\Entity\SubFileUpload;

/**
 * Class ApiController
 * @package Terminalbd\BankReconciliationBundle\Controller\Api
 * @Route("/api")
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class ApiController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/sales/payment/sync", name="br_sales_payment_sync_api")
     */
    public function syncSalesPayment(ParameterBagInterface $parameterBag, HttpClientInterface $client, Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $requestDate = $request->query->get('request-date');
        $requestDate = new \DateTime($requestDate);
        $requestDate = $requestDate->format('Y-m-d');

        $requestBankId = $request->query->get('bank-id');
        $requestReceiveAccount = $request->query->get('receive-account');
        $requestFileUploadId = $request->query->get('file-upload-id');


        $getBankInfo = $this->getDoctrine()->getRepository('App:Admin\Bank')->findOneBy(['id'=>$requestBankId]);
        $getBankSlug = $getBankInfo->getSlug();
        $file = $this->getDoctrine()->getRepository(FileUpload::class)->find($requestFileUploadId);
        $requestFileUpload = $this->getDoctrine()->getRepository(FileUpload::class)->find($requestFileUploadId);

//        dd($getBankInfo,$getBankSlug,$file,$requestFileUpload);


//        dd($getBankSlug,$file,$requestFileUpload);
        /*if ($request->getMethod() == 'POST' && null != $request->request->get('request-date')){
//            $requestBankId = $request->query->get('bank-id');
            dd('ok');
        }*/

//        dd($requestBankId);

//        if ($request->getMethod() == 'GET' && null != $request->query->get('request-date')){
//            $requestBankId = $request->query->get('bank-id');
//            $getBankInfo = $this->getDoctrine()->getRepository('App:Admin\Bank')->findOneBy(['id'=>$requestBankId]);
//            $getBankSlug = $getBankInfo->getSlug();
//            $requestReceiveAccount = $request->query->get('receive-account');
//            $requestFileUploadId = $request->query->get('file-upload-id');
//            $file = $this->getDoctrine()->getRepository(FileUpload::class)->find($requestFileUploadId);
//            $requestFileUpload = $this->getDoctrine()->getRepository(FileUpload::class)->find($requestFileUploadId);
//        }

        /*dd($requestFileUpload);*/

        $apiEndPoint = $parameterBag->get('salesDomain') . '/api/payments';
        $response = $client->request('GET', $apiEndPoint, [
            'headers' => [
                'X-API-KEY' => '79b8428a0dea686430a7f20ccbe857bd'
            ],
            'query' => [
                'request_date' => $requestDate,
//                'bank_id' => $requestBankId,
                'bank_slug' => $getBankSlug,
                'receive_account' => $requestReceiveAccount
            ]
        ]);

        $records = $response->toArray();

        $em = $this->getDoctrine()->getManager();
        foreach ($records as $record) {
            $findAgent = $this->getDoctrine()->getRepository(Agent::class)->findOneBy(['agentId' => $record['agentId'],'agentGroup'=>10]);
            $findBank = $this->getDoctrine()->getRepository(Bank::class)->findOneBy(['slug' => $record['bankSlag']]);
            $findBranch = $this->getDoctrine()->getRepository(BankBranch::class)->findOneBy(['branchCode' => $record['branchCode'], 'bank' => $findBank]);
            $existPayment = $this->getDoctrine()->getRepository(SalesPayment::class)->findOneBy(['paymentId' => $record['paymentId']]);

            if ($existPayment){
                continue;
            }

            if (!$findAgent){
                $findDistrict = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['oldId' => $record['districtId']]);
                $findUpozila = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['oldId' => $record['upozillaId']]);
                $findAgentGroup = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['name' => ucfirst(strtolower($record['agentType']))]);

                $newAgent = new Agent();
                $newAgent->setAgentId($record['agentId']);
                $newAgent->setDistrict($findDistrict);
                $newAgent->setUpozila($findUpozila);
                $newAgent->setEmail($record['email']);
                $newAgent->setPhone($record['phone']);
                $newAgent->setName($record['agentName']);
                $newAgent->setAgentGroup($findAgentGroup);
                $em->persist($newAgent);
                $em->flush();
                $findAgent = $newAgent;
            }

            if ($findBank && !$findBranch){
                $newBranch = new BankBranch();
                $newBranch->setBranchName($record['branchName']);
                $newBranch->setBranchCode($record['branchCode']);
                $newBranch->setBank($findBank);
                $newBranch->setStatus(true);
                $newBranch->setCreatedAt(new \DateTime('now'));
                $newBranch->setMobile(null);
                $em->persist($newBranch);
                $em->flush();
                $findBranch = $newBranch;
            }

            $salesPayment = new SalesPayment();
            $salesPayment->setDepositAmount($record['amount']);
            $salesPayment->setDepositDate(new \DateTime($record['depositDate']));
            $salesPayment->setStatus(true);
            $salesPayment->setCreatedAt($record['createdAt'] ? new \DateTime($record['createdAt']) : null);
            $salesPayment->setAgent($findAgent);
            $salesPayment->setBranch($findBranch);
            $salesPayment->setFileUpload($file);
            $salesPayment->setPaymentId($record['paymentId']);
            $salesPayment->setIsSales('yes');
            $em->persist($salesPayment);
            $em->flush();

        }

        if($requestFileUpload){
            foreach($requestFileUpload->getTransactions() as $transaction){
                if($transaction->getBranch()){
                    $this->switchTransactionReconciliation($requestFileUpload,$transaction);
                }
            }
            $this->InsertBankTransactionDataByDefault($requestFileUpload);
            $this->InsertSalesTransactionDataByDefault($requestFileUpload);
        }

        $this->addFlash('success', 'Branches have been updated & Synchronisation completed!');
        return $this->redirectToRoute('br_index', [
            'transactionDate' => $requestDate,
            'bank' => $getBankSlug,
            'accountType' => $requestReceiveAccount
        ], 307);
    }

    public function switchTransactionReconciliation(FileUpload $requestFileUpload,BankTransaction $record)
    {
        $salesDataByIdBranch = $this->getDoctrine()->getRepository(SalesPayment::class)->findBy(['fileUpload' => $requestFileUpload,'branch' => $record->getBranch()]);
        if ($salesDataByIdBranch) {
            $diffAmount = 0;
            /* @var $row SalesPayment */
            foreach ($salesDataByIdBranch as $row) {
                $diffAmount = ($row->getDepositAmount()-$record->getDeposit());
//                echo $row->getDepositAmount().'-'.$record->getDeposit().'='.$diffAmount.' <br>';
//                dd($row,$record);
                if($diffAmount == 0){
                    $this->InsertDataByIdAmountBranch($row, $record,'blue');
                }

                switch ($diffAmount) {
                    case $diffAmount > 0 && $diffAmount <= 150:
                        $this->InsertDataByIdAmountBranch($row, $record,'yellow');
                        break;
                    case $diffAmount >= 151 && $diffAmount <= 450:
                        $this->InsertDataByIdAmountBranch($row, $record,'purple');
                        break;
                    default:
                        break;
                }
            }
        }

    }

    public function InsertSalesTransactionDataByDefault($requestFileUpload){
        $em = $this->getDoctrine()->getManager();
        $salesData = $this->getDoctrine()->getRepository(SalesPayment::class)->findBy(['fileUpload' => $requestFileUpload,'bankReconciliation'=> null ]);
        foreach ( $salesData as $salesRow){
            $salesExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('salesPayment'=>$salesRow));
            if (empty($salesExist)) {
                $bankReconciliation = new Reconciliation();
                $bankReconciliation->setFileUpload($requestFileUpload);
                if ($salesRow->getBranch()){
                    $bankReconciliation->setBranch($salesRow->getBranch());
                }
                $bankReconciliation->setSalesPayment($salesRow);
                $bankReconciliation->setStatus(1);
                $bankReconciliation->setSalesPaymentAmount($salesRow->getDepositAmount());
                $bankReconciliation->setCreatedAt(new \DateTime('now'));
                $bankReconciliation->setAgent($salesRow->getAgent());
                $bankReconciliation->setActualAmount($salesRow->getDepositAmount());
                $bankReconciliation->setReconciliationFlag('red');
                $em->persist($bankReconciliation);
                $em->flush();

                $salesRow->setBankReconciliation($bankReconciliation);
                $em->persist($salesRow);
                $em->flush();
            }
        }
    }

    public function InsertBankTransactionDataByDefault($requestFileUpload){
        $em = $this->getDoctrine()->getManager();
        $bankTransData = $this->getDoctrine()->getRepository(BankTransaction::class)->findBy(['fileUpload' => $requestFileUpload,'bankReconciliation'=> null ]);
        foreach ($bankTransData as $transData) {
            $recordExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction' => $transData));
            if (empty($recordExist)) {
                $bankReconciliation = new Reconciliation();
                $bankReconciliation->setFileUpload($requestFileUpload);
                $bankReconciliation->setBankTransaction($transData);
                if ($transData->getBranch()){
                    $bankReconciliation->setBranch($transData->getBranch());
                }
                $bankReconciliation->setStatus(1);
                $bankReconciliation->setTransactionAmount($transData->getDeposit());
                $bankReconciliation->setCreatedAt(new \DateTime('now'));
//                $bankReconciliation->setActualAmount($transData->getDeposit());
                $bankReconciliation->setReconciliationFlag('red');
                $em->persist($bankReconciliation);
                $em->flush();

                $transData->setBankReconciliation($bankReconciliation);
                $em->persist($transData);
                $em->flush();
            }
        }
    }


    public function InsertDataByIdAmountBranch(SalesPayment $salesData,BankTransaction $record,$flug = ''){
//        dd($record->getFileUpload(),$salesData);
        $subFileExists = $this->getDoctrine()->getRepository(SubFileUpload::class)->findOneBy(['fileUpload'=>$record->getFileUpload()]);
//        dd($subFileExists);
        $em = $this->getDoctrine()->getManager();
        if ($subFileExists){
            //            dd('sub file esists');
            $recordExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction' => $record));
            $salesExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('salesPayment' => $salesData));
//            dd($recordExist,$salesExist);
            if (empty($recordExist) && $salesExist){
                if (empty($salesData->getBankTransaction())){
//                    dd($salesExist,$salesData, $record,$flug);
                    $this->updateSubFileData($salesExist,$salesData, $record,$flug);
                }
            }
        }else{
            $recordExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction' => $record));
            $salesExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('salesPayment' => $salesData));
            if (empty($recordExist) && empty($salesExist)) {
                $bankReconciliation = new Reconciliation();
                $bankReconciliation->setFileUpload($record->getFileUpload());
                $bankReconciliation->setBankTransaction($record);
                $bankReconciliation->setSalesPayment($salesData);
                $bankReconciliation->setBranch($salesData->getBranch());
                $bankReconciliation->setStatus(1);
                $bankReconciliation->setTransactionAmount($record->getDeposit());
                $bankReconciliation->setSalesPaymentAmount($salesData->getDepositAmount());
                $bankReconciliation->setDiffAmount($salesData->getDepositAmount() - $record->getDeposit());
                $bankReconciliation->setCreatedAt(new \DateTime('now'));
                $bankReconciliation->setAgent($salesData->getAgent());
                $bankReconciliation->setActualAmount($salesData->getDepositAmount());
                $bankReconciliation->setReconciliationFlag($flug);
//            $bankReconciliation->setApprovedBy($this->getUser());
//            dd($bankReconciliation);
                $em->persist($bankReconciliation);
                $em->flush();

                $salesData->setBankReconciliation($bankReconciliation);
                $salesData->setBankTransaction($record);
                $em->persist($salesData);
                $em->flush();

                $record->setBankReconciliation($bankReconciliation);
                $em->persist($record);
                $em->flush();
            }
        }
        /*dd('ok');
        if (count($subFileExists) == 0) {
            $recordExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction' => $record));
            $salesExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('salesPayment' => $salesData));
            if (empty($recordExist) && empty($salesExist)) {
                $bankReconciliation = new Reconciliation();
                $bankReconciliation->setFileUpload($record->getFileUpload());
                $bankReconciliation->setBankTransaction($record);
                $bankReconciliation->setSalesPayment($salesData);
                $bankReconciliation->setBranch($salesData->getBranch());
                $bankReconciliation->setStatus(1);
                $bankReconciliation->setTransactionAmount($record->getDeposit());
                $bankReconciliation->setSalesPaymentAmount($salesData->getDepositAmount());
                $bankReconciliation->setDiffAmount($salesData->getDepositAmount() - $record->getDeposit());
                $bankReconciliation->setCreatedAt(new \DateTime('now'));
                $bankReconciliation->setAgent($salesData->getAgent());
                $bankReconciliation->setActualAmount($salesData->getDepositAmount());
                $bankReconciliation->setReconciliationFlag($flug);
//            $bankReconciliation->setApprovedBy($this->getUser());
//            dd($bankReconciliation);
                $em->persist($bankReconciliation);
                $em->flush();

                $salesData->setBankReconciliation($bankReconciliation);
                $salesData->setBankTransaction($record);
                $em->persist($salesData);
                $em->flush();

                $record->setBankReconciliation($bankReconciliation);
                $em->persist($record);
                $em->flush();
            }
        }else{
//            dd('sub file esists');
            $recordExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction' => $record));
            $salesExist = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('salesPayment' => $salesData));
//            dd($recordExist,$salesExist);
            if (empty($recordExist) && $salesExist){
                if (empty($salesData->getBankTransaction())){
//                    dd($salesExist,$salesData, $record,$flug);
                    $this->updateSubFileData($salesExist,$salesData, $record,$flug);
                }
            }
        }*/
    }

    public function updateSubFileData(Reconciliation $reconciliation,SalesPayment $salesData, BankTransaction $record,$flug){
        $em = $this->getDoctrine()->getManager();
//        dd($reconciliation,$salesData,$record,$flug);

        $reconciliation->setBankTransaction($record);
        $reconciliation->setStatus(true);
        $reconciliation->setTransactionAmount($record->getDeposit());
        $reconciliation->setDiffAmount($salesData->getDepositAmount()-$record->getDeposit());
        $reconciliation->setCreatedAt(new \DateTime('now'));
        $reconciliation->setReconciliationFlag($flug);

        $em->persist($reconciliation);
//        $em->flush();

        $salesData->setBankTransaction($record);
        $salesData->setBankReconciliation($reconciliation);
        $em->persist($salesData);
//        $em->flush();

        $record->setBankReconciliation($reconciliation);
        $em->persist($record);
        $em->flush();
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/bank/branches", name="api_send_bank_branch")
     */
    public function sendBankBranches(Request $request)
    {
        if ($request->getMethod() == 'GET' && ($request->headers->get('Content-Type') == 'application/json')){
            $records = $this->getDoctrine()->getRepository(Api::class)->getBankBranches();

            $data['Status'] = 200;
            $data['Content-Type'] = $request->headers->get('Content-Type');
            $data['data'] = $records;

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($data));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;
        }else{
            return new JsonResponse([
                'message' => 'Invalied request',
                'status' => 500
            ]);
        }

    }

}