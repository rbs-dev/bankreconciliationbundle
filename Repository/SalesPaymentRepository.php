<?php

namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;


class SalesPaymentRepository extends EntityRepository
{

    public function getPayment($bank, $transactionDate)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.branch', 'branch');
        $qb->join('branch.bank', 'bank');
        $qb->leftJoin('e.agent', 'agent');
        $qb->select('e.depositAmount');
        $qb->addSelect('branch.branchCode');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');

        $qb->where('bank.id = :bankId')->setParameter('bankId', $bank->getId());
        $qb->andWhere('e.depositDate = :depositDate')->setParameter('depositDate', $transactionDate);
        $records = $qb->getQuery()->getArrayResult();
        $data = [];
        foreach ($records as $record) {
            $data[$record['branchCode']. '-' .$record['depositAmount']] = $record;
        }
        return $data;
    }

    public function getSalesDropDownData($bank,$transactionDate,$type){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.branch', 'branch');
        $qb->join('branch.bank', 'bank');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('agent.district', 'district');
        $qb->leftJoin('district.parent', 'region');
        $qb->select('e.id','e.depositAmount as depositAmount','e.depositDate as salesDate');
        $qb->addSelect('branch.branchCode','branch.id as branchId','branch.branchName as branchName');
        $qb->addSelect('fileUpload.accountType');
        $qb->addSelect('bank.id as bankId');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('region.name as regionName');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');

        $qb->where('bank.id = :bankId')->setParameter('bankId', $bank->getId());
        $qb->andWhere('fileUpload.accountType = :accountType')->setParameter('accountType', $type);
        #$qb->andWhere('fileUpload.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);
        $qb->andWhere('e.bankTransaction IS NULL');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }

    public function getSalesDropDownDataWithoutBank($transactionDate,$type){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.branch', 'branch');
        $qb->join('branch.bank', 'bank');
        $qb->join('e.fileUpload', 'fileUpload');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('agent.district', 'district');
        $qb->leftJoin('district.parent', 'region');

        $qb->select('e.id','e.depositAmount as depositAmount','e.depositDate as salesDate');
        $qb->addSelect('branch.branchCode','branch.id as branchId','branch.branchName as branchName');
        $qb->addSelect('fileUpload.accountType');
        $qb->addSelect('bank.id as bankId','bank.name as bankName');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('region.name as regionName');
        $qb->addSelect('agent.agentId', 'agent.name AS agentName', 'agent.mobile AS agentMobile', 'agent.address AS agentAddress');

        #$qb->where('fileUpload.transactionDate = :transactionDate')->setParameter('transactionDate',$transactionDate);
        $qb->where('fileUpload.accountType = :accountType')->setParameter('accountType', $type);
        $qb->andWhere('e.bankTransaction IS NULL');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }


    public function UnrecognisedSalesPayment($startDate,$endDate,$region,$agentid){
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.agent', 'agent');
        $qb->leftJoin('agent.district', 'district');
        $qb->leftJoin('district.parent', 'region');
        $qb->leftJoin('e.branch', 'branch');
        $qb->join('e.bankReconciliation', 'bankReconciliation');

        $qb->select('e.id AS salesId','e.depositAmount');
        $qb->addSelect('branch.branchName', 'branch.branchCode');
        $qb->addSelect('agent.name as AgentName','agent.agentId','e.depositDate');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('region.name as regionName');

        $qb->where('bankReconciliation.approvedBy IS NULL');
        $qb->andWhere('e.depositDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate);
        if (isset($region) && !empty($region)){
            $qb->andWhere('region.id = :id')->setParameter('id', $region);
        }
        if (isset($agentid) && !empty($agentid)){
            $qb->andWhere('agent.agentId = :agentId')->setParameter('agentId',$agentid);
        }
        $qb->orderBy('region.name','ASC');

        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function depotDataInsert($data){
        dd($data);
    }
}