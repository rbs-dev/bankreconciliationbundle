<?php


namespace Terminalbd\BankReconciliationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\SubFileUpload;
use Terminalbd\BankReconciliationBundle\Form\FileUploadFormType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BankReconciliationBundle\Form\SubFileUploadFormType;


/**
 * Class FileUploadController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Route("/file")
 */
class FileUploadController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @Route("/bank-statement-upload", name="br_bank_statement_upload")
     */
    public function bankStatementUpload(Request $request, TranslatorInterface $translator)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $fileUpload = new FileUpload();
        $allowFileType = ['xlsx'];

        $data = $this->getDoctrine()->getRepository(FileUpload::class)
            ->findBy(['type' => 'bank-statement'],['id' => 'desc']);
        $form = $this->createForm(FileUploadFormType::class, $fileUpload);
        $form->handleRequest($request);

        /*$exist = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(array('bank'=>$fileUpload->getBank(),'accountType'=>$fileUpload->getAccountType(),'transactionDate'=>$fileUpload->getTransactionDate()));*/
        if ($form->isSubmitted() && $form->isValid()){
            $transactionDate = $fileUpload->getTransactionDate()->format('d-m-Y');
            /*if($exist){
                $this->addFlash('error', $translator->trans('File Already Uploaded!'));
                return $this->redirectToRoute('br_bank_statement_upload');
            }else{*/
                $uploadedFile = $form['UploadFile']->getData();
                if (in_array($uploadedFile->getClientOriginalExtension(), $allowFileType)){
                    $em = $this->getDoctrine()->getManager();
                    $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

                    $newFileName = $form['bank']->getData()->getSlug() . '_TD_' . $transactionDate . '_CD_' . date('d-m-Y') . '-' . time() . '.' . $uploadedFile->getClientOriginalExtension();
                    $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/';
                    $uploadedFile->move(
                        $uploadDir,
                        $newFileName
                    );

                    $accountCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findOneBy(['bank'=>$form['bank']->getData()->getId(),'accountType'=>$form['accountType']->getData()]);
                    if ($accountCode) {
                        $code = $this->getDoctrine()->getRepository(BankAccountCode::class)->find($accountCode->getId());
                    }else{
                        $code = null;
                    }

                    $fileUpload->setFileName($newFileName);
                    $fileUpload->setType('bank-statement');
                    $fileUpload->setCreatedAt( new \DateTime('now'));
                    $fileUpload->setBankAccountCode($code);
                    $em->persist($fileUpload);
                    $em->flush();

                    $this->addFlash('success', $translator->trans('File Uploaded Successfully!'));
                    return $this->redirectToRoute('br_bank_statement_list');
                }else{
                    $this->addFlash('error', $translator->trans('Invalid File Format!'));
                    return $this->redirectToRoute('br_bank_statement_upload');
                }
//            }

        }
        return $this->render('@TerminalbdBankReconciliation/fileUpload/bank-statement.html.twig',[
            'form' => $form->createView(),
            'data' => $data,
        ]);
    }



    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @Route("/bank-statement-list", name="br_bank_statement_list")
     */
    public function bankStatementList(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $data = $this->getDoctrine()->getRepository(FileUpload::class)->findBy(['type' => 'bank-statement'],['id' => 'desc']);

        return $this->render('@TerminalbdBankReconciliation/fileUpload/bank-statement-list.html.twig',[
            'data' => $data,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @Route("/{id}/delete", name="br_file_delete")
     */
    public function delete(FileUpload $file)
    {
        $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/';
        $subUploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/sub-file/';

        if (file_exists($uploadDir.$file->getFileName())){
            $filesystem = new Filesystem();
            $filesystem->remove($uploadDir.$file->getFileName());
        }

        $subFile = $this->getDoctrine()->getRepository(SubFileUpload::class)->findAll(['fileUpload'=>$file]);
        if ($subFile){
            foreach ($subFile as $value){
                if (file_exists($subUploadDir.$value->getSubFileName())){
                    $filesystem = new Filesystem();
                    $filesystem->remove($subUploadDir.$value->getSubFileName());
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();

        $this->addFlash('success','File has been deleted!');
        return $this->redirectToRoute('br_bank_statement_upload');
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @Route("/custom-statement-create", name="br_custom_statement_create")
     */
    public function customStatementCreate(Request $request)
    {
        $fileUpload = new FileUpload();
        $form = $this->createForm(FileUploadFormType::class,$fileUpload)->remove('UploadFile');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $accountCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findOneBy(['bank'=>$form['bank']->getData()->getId(),'accountType'=>$form['accountType']->getData()]);
            if ($accountCode) {
                $code = $this->getDoctrine()->getRepository(BankAccountCode::class)->find($accountCode->getId());
            }else{
                $code = null;
            }

            $transactionDate = $fileUpload->getTransactionDate()->format('d-m-Y');
            $em = $this->getDoctrine()->getManager();
            $fileUpload->setType('bank-statement');
            $fileUpload->setBankAccountCode($code);
            $fileUpload->setCreatedAt( new \DateTime('now'));
            $fileUpload->setStatus(1);
            $em->persist($fileUpload);
            $em->flush();

            $this->addFlash('success','Custom Statement Added Successfully!');
            return $this->redirectToRoute('br_bank_statement_list');
        }
        return $this->render('@TerminalbdBankReconciliation/fileUpload/custom-statement-create.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @Route("/transacton/{id}/sub/file/upload", name="br_transaction_add_sub_file")
     */
    public function transactionSubFileUpload(Request $request, FileUpload $fileUpload)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');
        $bank = $fileUpload->getBank();

        $form = $this->createForm(SubFileUploadFormType::class,$fileUpload)->remove('bank');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $subFileUpload = new SubFileUpload();
            $allowFileType = ['xlsx'];
            $uploadedFile = $form['UploadFile']->getData();
            if (in_array($uploadedFile->getClientOriginalExtension(), $allowFileType)) {
                $em = $this->getDoctrine()->getManager();
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                $findSubFile = $this->getDoctrine()->getRepository(SubFileUpload::class)->findOneBy(['fileUpload'=>$fileUpload->getId()],['id'=>'desc']);
                if ($findSubFile){
                    $subFileSL = $findSubFile->getBatchName();
                    $str = substr(strrchr($subFileSL, '-'), 1);
                    $subFileSL = $str+1;
                }else{
                    $subFileSL = 1;
                }
                $transactionDate = $fileUpload->getTransactionDate()->format('d-m-Y');
                $batchNumberSlug = $transactionDate.'-'.$fileUpload->getId().'-'.$subFileSL;
//                $batchNumber = $transactionDate.'#'.$fileUpload->getId().'-'.$subFileSL;

                $fileName = substr($fileUpload->getFileName(), 0, strpos($fileUpload->getFileName(), '_'));
                $newFileName = $fileName . '_TD_' . $transactionDate . '_CD_' . date('d-m-Y') . '-BN-' . $batchNumberSlug . '.' . $uploadedFile->getClientOriginalExtension();
                $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/sub-file';
                $uploadedFile->move(
                    $uploadDir,
                    $newFileName
                );
            }

            $subFileUpload->setFileUpload($fileUpload);
            $subFileUpload->setStatus(false);
            $subFileUpload->setBatchName($batchNumberSlug);
            $subFileUpload->setCreatedAt( new \DateTime('now'));
            $subFileUpload->setSubFileName($newFileName);
            $subFileUpload->setUploadedBy($this->getUser());
            $em->persist($subFileUpload);
            $em->flush();

            return $this->redirectToRoute('br_transaction_import', [
                'id' => $fileUpload->getId(),
                'sub-file-id' => $subFileUpload->getId(),
                'sub-file'=>'yes'
            ], 307);


        }
        return $this->render('@TerminalbdBankReconciliation/fileUpload/add-sub-file.html.twig',[
            'fileData' =>$fileUpload,
            'form' => $form->createView(),
        ]);

    }

}