<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;

class BankAccontCodeFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $bank = $options['bank']->getBank()->getId();
        $builder
            ->add('bank', EntityType::class,[
                'class' => Bank::class,
                'placeholder' => 'Select Bank',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where('e.status = 1')
                    ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])

            ->add('branchId',ChoiceType::class,[
                'required' => true,
                'attr' => [
                    'class' => ' branchSearch'
                ],
                'mapped' => false,
                'multiple' => false,
//                'choices'=>['Dhanmondi'=>'34']
            ])

            ->add('accountType',ChoiceType::class,[
                'choices' => [
                    'Poultry' => 'POULTRY',
                    'Agro' => 'AGRO',
                    'Feeds' => 'FEED'
                ],
                'placeholder' => '---Select Account Type---',
            ])
            ->add('accountNo', TextType::class,[
                'required' => false,
            ])

            ->add('accountCode', TextType::class,[
                'required' => false,
            ])

            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled",
                    'checked' => "checked"
                ],
            ])
            ->add('submit', SubmitType::class)
            ->setMethod('post')
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BankAccountCode::class,
//            'allow_extra_fields' => true,
        ]);
    }



}