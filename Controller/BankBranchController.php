<?php


namespace Terminalbd\BankReconciliationBundle\Controller;


use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Form\BankBranchFormType;
use Terminalbd\BankReconciliationBundle\Form\SearchFormType;
use Terminalbd\BankReconciliationBundle\Repository\BankBranchRepository;

/**
 * Class BankBranchController
 * @package Terminalbd\BankReconciliationBundle\Controller
 */
class BankBranchController extends AbstractController
{
    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/branch-list", name="br_branch_list")
     */
    public function index(Request $request)
    {
        $filterBy = null;
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        $branch = null;
        if ($form->isSubmitted()){
            $filterBy = $form->getData();
            $branch = $form['bankBranch']->getData();
        }
        $records = $this->getDoctrine()->getRepository(BankBranch::class)->getBranches($filterBy,$branch);
        $data = $this->paginate($request, $records);
        return $this->render('@TerminalbdBankReconciliation/bankBranch/index.html.twig',[
            'data' => $data,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @param Request $request
     * @Route("/branch/create", name="br_branch_create")
     */
    public function addBranch(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5120M');

        $branch = new BankBranch();
        $form = $this->createForm(BankBranchFormType::class, $branch);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $branchName = $form['branchName']->getData();
            $branchCode = $form['branchCode']->getData();
            $bank = $form['bank']->getData();
            $uploadedFile = $form['bulkUploadFile']->getData();
            $allowFileType = ['xlsx'];


            if ($uploadedFile != null && $branchName == null && $branchCode == null){
                if (in_array($uploadedFile->getClientOriginalExtension(), $allowFileType)){
                    $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $uploadedFile->getClientOriginalExtension();
                    $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/branchExcel/';

                    $uploadedFile->move(
                        $uploadDir,
                        $originalFilename
                    );

                    $reader = new Xlsx();
                    $spreadSheet = $reader->load($uploadDir . $originalFilename);
                    $excelSheet = $spreadSheet->getActiveSheet();
                    $allData = $excelSheet->toArray();
//                    dd($allData);
                    $keys = array_filter(array_shift($allData));
                    $keysLength = count($keys);
                    $data = [];
                    foreach ($allData as $value){
                        $data[] = array_combine($keys, array_slice($value, null, $keysLength));
                    }
//                    dd($data);
                    foreach ($data as $item) {
//dd($item);
                        if (isset($item['BranchName']) && $item['BranchName']!='' && $item['BranchName']!=null){
//                            dd($item['BranchName']);
//                            $existBranch = $this->getDoctrine()->getRepository(BankBranch::class)->findOneBy(['branchCode' => trim($item['BranchCode']),'branchName' => trim($item['BranchName']), 'bank' => $bank]);
                            $existBranch = $this->getDoctrine()->getRepository(BankBranch::class)->findOneBy(['branchName' => trim($item['BranchName']), 'bank' => $bank]);
                            if ($existBranch){
                                $branch=$existBranch;
                            }else{
                                $branch = new BankBranch();
                            }

                            if (isset($item['District'])){
                                $District = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['name'=>$item['District']]);
                                if (empty($District)){
                                    $District = null;
                                }
                            }else{
                                $District = null;
                            }

                            $branch->setBank($bank);
                            $branch->setBranchCode(trim($item['BranchCode']));
                            $branch->setBranchName(trim($item['BranchName']));
                            $branch->setCreatedAt(new \DateTime('now'));
                            $branch->setDistrict($District);
                            $branch->setStatus(1);
                            $em->persist($branch);
                            $em->flush();
                        }
                    }
                    if (file_exists($uploadDir.$originalFilename)){
                        $filesystem = new Filesystem();
                        $filesystem->remove($uploadDir.$originalFilename);
                    }
                    $this->addFlash('success', 'New Branches added!');
                    return $this->redirectToRoute('br_branch_list');
                }else{
                    $this->addFlash('error', 'Invalid file format!');
                    return $this->redirectToRoute('br_branch_create');
                }
            }elseif ($uploadedFile == null && $branchName != null){
                $branch->setCreatedAt(new \DateTime('now'));
                $em->persist($branch);
                $em->flush();
                $this->addFlash('success', 'New Branch added!');
                return $this->redirectToRoute('br_branch_list');
            }elseif ($uploadedFile == null && $branchName == null && $branchCode == null){
                $this->addFlash('error', 'Invalid entry!');
                return $this->redirectToRoute('br_branch_create');
            }elseif ($uploadedFile == null && $branchName == null){
                $this->addFlash('error', 'Branch name must not empty!');
                return $this->redirectToRoute('br_branch_create');
            }else{
                $this->addFlash('error', 'You can not add single branch and bulk upload from excel file together!');
                return $this->redirectToRoute('br_branch_create');
            }
        }

        return $this->render('@TerminalbdBankReconciliation/bankBranch/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN')")
     * @param Request $request
     * @Route("/branch/{id}/update", name="br_branch_update")
     */
    public function BranchUpdate(Request $request,BankBranch $branch)
    {
        $form = $this->createForm(BankBranchFormType::class, $branch)->remove('bulkUploadFile');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $branchName = $form['branchName']->getData();
            $branchCode = $form['branchCode']->getData();
            $bank = $form['bank']->getData();

            $branch->setBank($bank);
            $branch->setBranchCode($branchCode);
            $branch->setBranchName($branchName);
            $branch->setUpdatedAt(new \DateTime('now'));
            $branch->setStatus(1);
            $em->persist($branch);
            $em->flush();


            $this->addFlash('success', 'Branch Updated Successfully!');
            return $this->redirectToRoute('br_branch_list');

        }
        return $this->render('@TerminalbdBankReconciliation/bankBranch/create.html.twig',[
            'form' => $form->createView(),
            'branch' => $branch
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @param Request $request
     * @Route("/{bank}/branches", name="br_branch_depend_on_bank", options={"expose"=true})
     */
    public function branches(Request $request, Bank $bank)
    {
        $branches = [];
        /* @var BankBranch $branch*/
        foreach ($bank->getBranches() as $branch){
            $branches[$branch->getId()]= $branch->getBranchName();
        }
        return new JsonResponse($branches);

    }


    /**
     * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_ADMIN') or is_granted('ROLE_BR_USER') ")
     * @Route("/search/branch", methods={"GET"}, name="branch_search_select2", options={"expose"=true})
     */
    public function searchSelect2(Request $request)
    {
        $branchKey= $request->query->get('q');
        $bank= $request->query->get('bank');
        $branchs = $this->getDoctrine()->getRepository(BankBranch::class)->getBranchSelect2($branchKey,$bank);
//        dd($branchs);
        return new JsonResponse($branchs);
    }
}