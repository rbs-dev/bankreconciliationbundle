<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;


class SmsConvertRepository extends EntityRepository
{
    public function GetAllSmsConvert(){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->leftJoin('e.branch','branch');
        $qb->join('e.agent','agent');
        $qb->leftJoin('agent.upozila','upozila');
        $qb->leftJoin('agent.district','district');
        $qb->join('e.depotId','depot');
        $qb->select('e.id','e.transactionDate','e.status','e.amount','e.type','e.isReconciliation');
        $qb->addSelect('bank.name as bankName','bank.slug as bankSlug');
        $qb->addSelect('branch.branchName as branchName');
        $qb->addSelect('agent.name as agentName','agent.mobile','agent.address');
        $qb->addSelect('upozila.name as upozilaName');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('depot.name as depotName');
        $qb->where('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('e.id','DESC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function recognisedSmsConvert($isReconciliation,$transactionDate,$type,$bank,$depot){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->leftJoin('e.branch','branch');
        $qb->join('e.agent','agent');
        $qb->leftJoin('agent.upozila','upozila');
        $qb->leftJoin('agent.district','district');
        $qb->leftJoin('e.createdBy', 'createdBy');
        $qb->leftJoin('e.approvedBy', 'approvedBy');
        $qb->leftJoin('e.depotId', 'depot');

        $qb->select('e.id','e.transactionDate','e.status','e.amount','e.type','e.isReconciliation');
        $qb->addSelect('bank.name as bankName','bank.slug as bankSlug');
        $qb->addSelect('branch.branchName as branchName');
        $qb->addSelect('agent.name as agentName','agent.mobile','agent.address','agent.agentId');
        $qb->addSelect('upozila.name as upozilaName');
        $qb->addSelect('district.name as districtName');
        $qb->addSelect('createdBy.name as userName','createdBy.mobile as userMobile');
        $qb->addSelect('approvedBy.name as approveName','approvedBy.mobile as approveMobile');
        $qb->addSelect('depot.name as depotName');

        $qb->where('e.status = :status')->setParameter('status', 1);
        $qb->andWhere('e.transactionDate = :transactionDate')->setParameter('transactionDate', $transactionDate);
        $qb->andWhere('e.isReconciliation = :isReconciliation')->setParameter('isReconciliation', $isReconciliation);
        if (!empty($type)){
            $qb->andWhere('e.type = :type')->setParameter('type', $type);
        }
        if (!empty($bank)){
            $qb->andWhere('e.bank = :bank')->setParameter('bank', $bank);
        }
        if (!empty($depot)){
            $qb->andWhere('e.depotId = :depotId')->setParameter('depotId', $depot);
        }
        $qb->orderBy('e.id','DESC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }
}