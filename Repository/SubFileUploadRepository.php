<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;


class SubFileUploadRepository extends EntityRepository
{
    public function uploadData(){
        $qb = $this->createQueryBuilder('e');
        $qb->innerJoin('e.transactions', 'transactions');
//        $qb->leftJoin('e.bank', 'bank');
//        $qb->leftJoin('e.reconciliations', 'reconciliations');
        $qb->select('e.id','e.type','e.accountType','e.fileName','e.status','e.transactionDate');
//        $qb->addSelect('bank.name','bank.id');
        $qb->addSelect('sum(transactions.deposit) as depositeTotal');

        $qb->where('e.type = :type')->setParameter('type', 'bank-statement');
        $qb->orderBy('e.id','DESC');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }
    public function allUploadFileWithTotal(){
        $qb = $this->createQueryBuilder('e');
//        $qb->leftJoin('e.reconciliations','reconciliations');
        $qb->join('e.bank','bank');
        $qb->join('e.transactions','transactions');

        $qb->select('e.id','e.type','e.accountType','e.fileName','e.status','e.transactionDate','e.createdAt');
//        $qb->addSelect('sum(reconciliations.transactionAmount) as depositeTotal');
        $qb->addSelect('sum(transactions.deposit) as depositeTotal');
        $qb->addSelect('bank.name as bankName');

        $qb->where('e.type = :type')->setParameter('type', 'bank-statement');
        $qb->orderBy('e.id','DESC');
//        $qb->groupBy('reconciliations.fileUpload');
        $qb->groupBy('transactions.fileUpload');
        $records = $qb->getQuery()->getArrayResult();

        return $records;
    }
}